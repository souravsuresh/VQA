from django.urls import path

from . import views

urlpatterns = [
    path('<str:img_path>/<str:test_ques>', views.index, name='main'),
]