import keras
#from config import *
import basic.config as config
from keras.applications.resnet50 import ResNet50
from keras.applications.vgg16 import VGG16
from keras.applications.xception import Xception

from keras.layers import *
#from config import *
import json
from PIL import Image
import scipy.misc
from keras.optimizers import SGD
from keras.utils import plot_model
from os import walk
import numpy as np
import warnings
from keras.models import Model,Sequential
from keras.layers import Flatten, Dense, Input
from keras.layers import Convolution2D, MaxPooling2D
from keras.preprocessing import image
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.utils.data_utils import get_file
from keras import backend as K
from keras.utils import plot_model
import keras
from numpy import asarray
from numpy import zeros
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import pickle
from keras.layers import Flatten, Dense, Input
from keras.models import Sequential, Model
from keras.layers.core import Reshape, Activation, Dropout
from keras.layers import LSTM, Merge, Dense, Embedding, Multiply
from keras.models import load_model
import tensorflow as tf
graph = tf.get_default_graph()
def pop(self):
    '''
      Removes a layer instance on top of the layer stack.
    '''
    if not self.outputs:
        raise Exception('Sequential model cannot be popped: model is empty.')
    else:
        self.layers.pop()
        if not self.layers:
            self.outputs = []
            self.inbound_nodes = []
            self.outbound_nodes = []
        else:
            self.layers[-1].outbound_nodes = []
            self.outputs = [self.layers[-1].output]
        self.built = False
def predict(img_array):
    return image_model.predict(img_array)

if config.model=="vgg16":
    image_model=VGG16(include_top=True, weights='imagenet', input_tensor=None, input_shape=None, pooling=None)  
    pop(image_model)
    pop(image_model)
    image_model.compile(loss='categorical_crossentropy',optimizer="sgd")

elif config.model == "resnet50":
    image_model=ResNet50(weights='imagenet')  
    pop(image_model)
    #pop(image_model)
    image_model.compile(loss='categorical_crossentropy',optimizer="sgd")

else:
    image_model=Xception(include_top=True, weights='imagenet')
    pop(image_model)
    #pop(image_model)
    image_model.compile(loss='categorical_crossentropy',optimizer="sgd")
image_model.summary()
#--------PREPROCESSING-------------------

def load_and_scale_imgs(img_path,norm_image=False):
    print(img_path)
    imgs=scipy.misc.imresize(scipy.misc.imread(img_path), (224, 224,3))
    imgs=np.array(imgs)
    if imgs.shape==(224,224):
      return []
    imgs=np.expand_dims(imgs, axis=0)
    global graph
    with graph.as_default():
        img_feature = predict(imgs)
    if norm_image:
        tem = np.sqrt(np.sum(np.multiply(img_feature, img_feature)))
        if config.model=="vgg16":
              img_feature = np.divide(img_feature, np.tile(tem,(1,4096)))
        else:
              img_feature = np.divide(img_feature, np.tile(tem,(1,2048)))
    return np.array(img_feature)



#image_model=keras.applications.vgg16.VGG16(include_top=True, weights='imagenet', input_tensor=None, input_shape=None, pooling=None)  
#---------IMAGE CHANNEL=======

def get_image_model(num_mlp_units=1024,activation_function="tanh"):
    new_model = Sequential() #new model
    if config.model=="vgg16":
        new_model.add(Reshape((4096,), input_shape=(4096,)))
    else:
        new_model.add(Reshape((2048,), input_shape=(2048,)))

    new_model.add(Dense(num_mlp_units,kernel_initializer='random_uniform'))
    new_model.add(Activation(activation_function))
    #new_model.add(Dropout(0.5))
    return new_model
