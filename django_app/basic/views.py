from django.shortcuts import render

# Create your views here.
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
from keras.layers import *
import json
from PIL import Image
import scipy.misc
import numpy as np
from keras.preprocessing import image
from numpy import asarray
from numpy import zeros
import pickle
import os

from basic.language_model import *
from basic.image_model import *
from random import randint
import tensorflow as tf
graph = tf.get_default_graph()
top_answer_path="basic/subset/top1000answers"
final = load_model('basic/vgg16_glove_lstm_20epochs.h5')
top_answers=open(top_answer_path,"rb")
top_answers=pickle.load(top_answers)
top_answers=top_answers[:1000]


def get_images(request,img_path):
    if img_path=="1":
        files=os.listdir("basic/train_img")
        # res=[
        #     '/static/basic/train_img/COCO_train2014_000000578986.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000092366.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000491170.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000283678.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000202914.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000550009.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000177109.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000164935.jpg',
        #     '/static/basic/train_img/COCO_val2014_000000462728.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000067871.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000019955.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000067199.jpg',
        #     '/static/basic/train_img/COCO_train2014_000000172176.jpg'
        # ]
        num_images_ret=5
        ret=[]
        i=0
        while i<num_images_ret:
            rand=randint(0,len(files)-1)
            st="/static/basic/train_img/"+files[rand]
            if st not in ret:
                ret.append(st)
                i+=1
        # #return {"images":",".join(ret)}
        f=open("temp","wb")
        pickle.dump(ret,f)
    else:
        ret=['/static/basic/app_test_imgs/2.jpg','/static/basic/app_test_imgs/1.jpg']
        f=open("temp","wb")
        pickle.dump(ret,f)
    print(ret)
    return ret
   
def index(request,img_path,test_ques):
        
        # if not request.POST:
        #     return render(request,'basic/index.html',{"images":images})
        # img_path=request.POST.get('img_path')
        # test_ques=request.POST.get('test_ques')
        if img_path=="1" or img_path=="2" or test_ques=="1":
            images=get_images(request,img_path)
            return render(request,'basic/index.html',{"images":images,"answer":{}})
        else:
            img=open("temp","rb")
            images=pickle.load(img)
        print(img_path)
        print(test_ques)
        if len(img_path.split(".")[0])!=1:
            test_img=load_and_scale_imgs("basic/train_img/"+img_path,True)
        else:
            test_img=load_and_scale_imgs("basic/static/basic/app_test_imgs/"+img_path,True)
        #f1=open("tokenizer","rb")
        # tokenizer=pickle.load(f2)
        # f2=open("max_ques_length","rb")
        # max_ques_length=pickle.load(f2)
        tokenizer=get_tokenizer(questions)
        max_ques_length=get_max_question_length(questions)
        encoded_test_ques=get_encoded_questions(tokenizer,[test_ques])
        padded_test_ques = get_padded_questions(encoded_test_ques, max_ques_length)
        global graph
        with graph.as_default():
            l=(final.predict([test_img,np.array(padded_test_ques)]))
        ll=l[0]
        final_res=[]
        for i in range(len(ll)):
            final_res.append([top_answers[i],ll[i]])
        final_res.sort(key=lambda x:x[1],reverse=True)
        ret=[]
        cnt=1
        for i in final_res[:5]:
                ret.append([i[0],"{0:.2f}".format(round(i[1]*100,2))])
                cnt+=1
        print(ret)
        return render(request,'basic/index.html',{"images":images,"answer":ret})

