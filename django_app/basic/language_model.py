from keras.layers import *
#from config import *
import basic.config as config
import json
from PIL import Image
import scipy.misc
from keras.optimizers import SGD
from keras.utils import plot_model
from os import walk
import numpy as np
import warnings
from keras.models import Model,Sequential
from keras.layers import Flatten, Dense, Input
from keras.layers import Convolution2D, MaxPooling2D
from keras.preprocessing import image
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.utils.data_utils import get_file
from keras import backend as K
from keras.utils import plot_model
import keras
from numpy import asarray
from numpy import zeros
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import pickle
from keras.layers import Flatten, Dense, Input
from keras.models import Sequential, Model
from keras.layers.core import Reshape, Activation, Dropout
from keras.layers import GRU,LSTM, Merge, Dense, Embedding, Multiply
from keras.models import load_model
f2=open("basic/subset/preprocessed_questions","rb")
questions=pickle.load(f2)

def get_max_question_length(list_of_questions):
    list_of_words_per_question = [len(question.split()) for question in list_of_questions]
    return max(list_of_words_per_question)

def get_glove_vectors(path = 'glove.6B.300d.txt'):
    embeddings_index = dict()
    f = open(path)
    for line in f:
        values = line.split() #split a line by space
        word = values[0] #first element is a string of the word
        coefs = asarray(values[1:], dtype='float32') #rest of the values are 300d floats
        embeddings_index[word] = coefs #creating a dictionary map
    f.close()
    return embeddings_index

def get_tokenizer(list_of_questions):
    t = Tokenizer()
    t.fit_on_texts(list_of_questions)
    return t

def get_vocab_size(tokenizer_obj):
    return len(tokenizer_obj.word_index) + 1


def get_encoded_questions(tokenizer_obj,list_of_questions):
    return tokenizer_obj.texts_to_sequences(list_of_questions)


def get_padded_questions(encoded_questions, max_question_length):
    padded_ques = pad_sequences(encoded_questions, maxlen=max_question_length, padding='post')
    return padded_ques    


def get_embedding_matrix(tokenizer_obj, embeddings_dict, vocab_size, embedding_dim = 300):
    embedding_matrix = zeros((vocab_size, embedding_dim))
    for word, i in tokenizer_obj.word_index.items():
        embedding_vector = embeddings_dict.get(word)
        if embedding_vector is not None: #checking if there exists a vector for the given word in the GloVe corpus
            embedding_matrix[i] = embedding_vector
    return embedding_matrix


def get_language_model_wrapper(vocab_size, embedding_matrix, max_question_len, 
                          embedding_dim = 300, num_lstm_mem_units = 512, 
                          num_mlp_units = 1024, activation_function = 'tanh'):
    model = Sequential()
    e = Embedding(vocab_size, embedding_dim, weights=[embedding_matrix], input_length=None, trainable=False)
    model.add(e)
    #model.add(LSTM(num_lstm_mem_units, return_sequences=True))
    if config.language_model=="lstm":
        for i in range(config.num_lstm-1):
            model.add(LSTM(num_lstm_mem_units, return_sequences=True,input_shape=(None, embedding_dim)))
        model.add(LSTM(num_lstm_mem_units,return_sequences=False)) #need to check whether return sequences must be true or false
    elif config.language_model=="gru":
        for i in range(config.num_lstm-1):
            model.add(GRU(num_lstm_mem_units, return_sequences=True,input_shape=(None, embedding_dim)))
        model.add(GRU(num_lstm_mem_units,return_sequences=False)) #need to check whether return sequences must be true or fal

    model.add(Dense(num_mlp_units,name="dense_lstm",kernel_initializer='random_uniform'))
    model.add(Activation(activation_function))
    if config.dropout_lstm!=-1:
        model.add(Dropout(config.dropout_lstm))
    return model

def get_language_model(): 
    max_ques_length = get_max_question_length(questions)
    tokenizer = get_tokenizer(questions)  
    encoded_ques = get_encoded_questions(tokenizer,questions) # not required for the lang model; its reqd for the model.fit function
    padded_ques = get_padded_questions(encoded_ques, max_ques_length)
    glove_dict = get_glove_vectors(config.word_vector)
    vocab_size = get_vocab_size(tokenizer)
    embedding_mat = get_embedding_matrix(tokenizer,glove_dict, vocab_size, config.embedding_dim)


    language_model = get_language_model_wrapper(vocab_size, embedding_mat, max_ques_length, config.embedding_dim, config.num_lstm_units, config.num_mlp_units, config.lstm_mlp_activation)
    return language_model