from django.shortcuts import render
from django.shortcuts import render

# Create your views here.
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
from keras.layers import *
import json
from PIL import Image
import scipy.misc
import numpy as np
from keras.preprocessing import image
from numpy import asarray
from numpy import zeros
import pickle
import os
from basic.language_model import *
from basic.image_model import *
from random import randint
import tensorflow as tf
# Create your views here.
import tensorflow as tf
graph = tf.get_default_graph()
top_answer_path="basic/subset/top1000answers"
final = load_model('basic/vgg16_glove_lstm_20epochs.h5')
top_answers=open(top_answer_path,"rb")
top_answers=pickle.load(top_answers)
top_answers=top_answers[:1000]

def get_images(request):
    files=os.listdir("basic/train_img")
    num_images_ret=1
    ret=[]
    i=0
    while i<num_images_ret:
        rand=randint(0,len(files)-1)
        st="/static/basic/train_img/"+files[rand]
        if st not in ret:
            ret.append(st)
            i+=1
    return ret
def predict(img_path,question):
    test_img=load_and_scale_imgs("basic/train_img/"+img_path,True)
    #f1=open("tokenizer","rb")
    # tokenizer=pickle.load(f2)
    # f2=open("max_ques_length","rb")
    # max_ques_length=pickle.load(f2)
    tokenizer=get_tokenizer(questions)
    max_ques_length=get_max_question_length(questions)
    encoded_test_ques=get_encoded_questions(tokenizer,[question])
    padded_test_ques = get_padded_questions(encoded_test_ques, max_ques_length)
    global graph
    with graph.as_default():
    
        l=(final.predict([test_img,np.array(padded_test_ques)]))
        ll=l[0]
        final_res=[]
        for i in range(len(ll)):
            final_res.append([top_answers[i],ll[i]])
        final_res.sort(key=lambda x:x[1],reverse=True)
        #ret=[]
        #cnt=1
        return final_res
def index(request,level,img_path):
    # if not request.POST:
    #     return render(request,'basic/index.html',{"images":images})
    # img_path=request.POST.get('img_path')
    # test_ques=request.POST.get('test_ques')
    if img_path=="1":
        images=get_images(request)
        # if level=
        # return render(request,'game/index.html',{"images":images,"answer":{}})
    
        # img=open("temp","rb")
        # images=pickle.load(img)
    img_path=images[0].split("/")[-1]
    if level==1:
        test_ques=["What is this?"]
    elif level==2:
        p=predict(img_path,"What is this?")
        if p[0][0]=="sport":
            test_ques=["What sport is this?"]
        else:
            test_ques=["What animal is this?"]
    res={}
    
    for ques in test_ques:   
        print(img_path)
        print(ques)
        final_res=predict(img_path,ques)
        res[ques]=[final_res[0][0],float(final_res[0][1])]
    res=[[key,res[key]] for key in res]
    res.sort(key=lambda x:x[1][1],reverse=True)
    print(res)
    return render(request,'game/index.html',{"images":images,"answer":res[0][1],"question":res[0][0]})