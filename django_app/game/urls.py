from django.urls import path

from . import views

urlpatterns = [
    path('<int:level>/<str:img_path>', views.index, name='main'),
]