# Visual Question Answering(VQA)

Free-form and Open-ended Question Answering system for the Visual content. Given an image and a natural
language question about the image, the task is to provide an accurate natural language answer.

This repository shows one of the applications of VQA task i.e `Teaching Aid for young children` 

## Getting Started

The repository mainly contains 2 directories:
* Code for building the VQA model
* Django application for implementing the use case of `Teaching Aid for young children`

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

All the dependencies are noted in requirements file
```
pip3 install requirements.txt
```


## VQA model



### Generating Subset under context

From the given VQA [dataset](http://www.visualqa.org/vqa_v1_download.html) we generated smaller subset by having context. Currently we have chosen context to be sports and animals

```
python3 generate_subset_context.py
```

### Pre-Processing

Clearly we cannot provide images and words as input to neural network as it understands only numbers. So we need to convert them to numbers, we use word2vec models such as GLoVe as part of language channel. For images we need to resize to fixed size(224,224)

```
python3 preprocessing.py
```

### Generating VQA model

VQA model contains 2 channels, one for processing image(`image_model.py`) and other to process the question(`language_model.py`). Later these 2 channels are combined with simple point-wise multiplication and the classification with top-1000 answer is done using softmax.

The model can be tweaked by changing configuration in `config.py` file

```
python3 main.py
```

## Django Application

  Contains 3 pages:
  * home (`django_app/home`) - has video which explains what is VQA
  * game (`django_app/game`) - has 2 levels where user needs to input the answer for question asked relevant to the image
  * basic-demo (`django_app/basic`) - basic demo where we can select an image and ask question where the system will respond with an answer

To use the application run: 

```
python3 manage.py 127.0.0.1:8080
```
Note: Before running the django application ensure that you are inside the `django_app` directory

## Deployment

The application was hosted on Apache2 Web server(Please follow the instructions given [here](https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-apache-and-mod_wsgi-on-ubuntu-14-04))


## Built With

* [Django](https://docs.djangoproject.com/en/2.0/) - The web framework used
* [Keras](https://keras.io/) - Module for implementing deep learning model


## Authors

* **[Sourav Suresh](https://gitlab.com/souravsuresh)** 
* **[Varun N Rao](https://gitlab.com/varunnrao)**

