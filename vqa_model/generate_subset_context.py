
#-------------- genrating data under context -------------------------
import json

def get_answers(answer_dir_path):
  files=["train2014"]
  for fl in files:
    f=open(answer_dir_path,"r")
    f=json.loads(list(f)[0])
    res=[]
    
    for answers in f["annotations"]:
       if True:
          final_ans={}
          l={}
          for ans in answers["answers"]:
              if ans["answer"] not in l:
                l[ans["answer"]]=[0,[ans["answer_confidence"]]] 
              l[ans["answer"]][0]+=1
              l[ans["answer"]][1].append(ans["answer_confidence"])
          l=[[i,l[i]] for i in l]
          l.sort(key=lambda x:x[1][0])
          print(l)
          mx=l[-1][1][0]
          c=0
          for i in l:
            if i[1][0]==mx:
              final_ans[i[0]]=[mx,i[1][1].count("yes")]
              c+=1
          if c>1:
            mx=-1
            key=-1
            for i in final_ans:
               if final_ans[i][1]>mx:
                     mx=final_ans[i][1]
                     key=i
            res.append({"answers":[key,mx],"question_id":answers["question_id"]})				
          else:
            key=list(final_ans.keys())[0]
            res.append({"answers":[key,final_ans[key][0]],"question_id":answers["question_id"]})

    new_file=open("ca_"+fl,"w")
    new_file.write(json.dumps(res))     

get_answers("mscoco_train2014_annotations.json")

animals='''
alligator
ant
bear
bee
bird
camel
cat
cheetah
chicken
chimpanzee
cow
crocodile
deer
dog
dolphin
duck
eagle
elephant
fish
fly
fox
frog
giraffe
goat
goldfish
hamster
hippopotamus
horse
kangaroo
kitten
lion
lobster
monkey
octopus
owl
panda
pig
puppy
rabbit
rat
scorpion
seal
shark
sheep
snail
snake
spider
squirrel
tiger
turtle
wolf
zebra
'''
animal_list=animals.split("\n")[1:-1]
sports_list=["tennis","baseball","skiing","surfing","football"]

import nltk
nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()

import pickle
f=open("ca_train2014")
f=json.loads(list(f)[0])
final_res={}
cnt=1
answers={}
context_list=animal_list+sports_list

for ans in f:
  answer=lemmatizer.lemmatize(ans["answers"][0])
  if answer in context_list:
    answers[ans["question_id"]]=answer
  cnt+=1
new_file=open("animals","wb")
pickle.dump(answers,new_file)


f=open("ca_train2014")
f=json.loads(list(f)[0])
final_res={}
cnt=1
answers={}
for ans in f:
  answers[ans["question_id"]]=ans["answers"][0]
new_file=open("question_answer_mapping","wb")
pickle.dump(answers,new_file)  

#-------------- End of Getting context-------------------------

from os import listdir,system
from random import randint
from os.path import isfile, join
import sys 
import json
import pickle
import random
random.seed( 3 )

def get_questions(questions_dir_path):
    files=["subset/train2014_subset"]
    for fl in files:
        f=open(fl)
        f=list(f)[0].split(",")
        print(f)
        ff=[]
        animals=open("animals","rb")
        animals=pickle.load(animals)
        for i in f:
          if len(i.split("_")) > 1:
            ff.append(int(i.split("_")[-1][:-4]))
        f=ff[:]
        print(len(f))
        questions_file_content=open(questions_dir_path)
        questions=(json.loads(list(questions_file_content)[0])["questions"])
        final=[]
        print(len(questions))
        imgs=[]
        for j in questions:
          if j["question_id"] in animals.keys():
            imgs.append(j["image_id"])
        for j in questions:
            if j["image_id"] in imgs:
                final.append(j)
        nf=open("subset/q_train2014","w")
        nf.write(json.dumps(final))

def get_answers(answer_dir_path):
    files=["train2014"]
    for fl in files:
      f=open("subset/q_"+fl,"r")
      f=json.loads(list(f)[0])
      q_ids=[]
      for i in f:
         q_ids.append(i["question_id"])
      f=open(answer_dir_path,"r")
      f=json.loads(list(f)[0])
      res=[]
      print(fl)
      for answers in f["annotations"]:
         if answers["question_id"] in q_ids:
                final_ans={}
                l={}
                for ans in answers["answers"]:
                  if ans["answer"] not in l:
                     l[ans["answer"]]=[0,[ans["answer_confidence"]]] 
                  l[ans["answer"]][0]+=1
                  l[ans["answer"]][1].append(ans["answer_confidence"])
                l=[[i,l[i]] for i in l]
                l.sort(key=lambda x:x[1][0])
                mx=l[-1][1][0]
                c=0
                for i in l:
                  if i[1][0]==mx:

                    final_ans[i[0]]=[mx,i[1][1].count("yes")]
                    c+=1
                if c>1:
                  mx=-1
                  key=-1
                  for i in final_ans:
                     if final_ans[i][1]>mx:
                           mx=final_ans[i][1]
                           key=i
                  res.append({"answers":[key,mx],"question_id":answers["question_id"]})       
                else:
                  key=list(final_ans.keys())[0]
                  res.append({"answers":[key,final_ans[key][0]],"question_id":answers["question_id"]})

      new_file=open("subset/ca_"+fl,"w")
      new_file.write(json.dumps(res))     

def get_top1000_answers(answer_dir_path):
    f=open("subset/q_train2014","r")
    f=json.loads(list(f)[0])
    q_ids=[]
    for i in f:
      q_ids.append(i["question_id"])   
    f=open(answer_dir_path,"r")
    f=json.loads(list(f)[0])
    res=[]
    for answers in f["annotations"]:
      if answers["question_id"] in q_ids:
         res.append(answers)
    ans={}
    for i in res:
      for answer in i["answers"]:
        if answer["answer"] not in ans:
          ans[answer["answer"]]=0
        ans[answer["answer"]]+=1
    ans=[[i,ans[i]] for i in ans ]
    ans.sort(key=lambda x:x[1],reverse=True)
    if len(ans)<1000:
      print("Please increase the dataset_size as you have not covered top1000 of answers")
    ans=ans[:999]
    ans=[i[0] for i in ans]
    ans.append("Dont Know")
    system("touch subset/top1000answers")
    f=open("subset/top1000answers","wb")
    pickle.dump(ans,f)
    system("touch subset/top1000answers_to_view")
    ff=open("subset/top1000answers_to_view","w")
    for i in ans:
      ff.write(i+"\n")
    f.close()
    ff.close()
    
system("mkdir subset")     
files = [[f for f in listdir(dir)] for dir in ["train2014"]]
files=files[0]
temp_file=open("subset/train2014_subset","w")
for k in range(len(files)-1):
        temp_file.write(files[k]+",")
temp_file.write(files[-1])
temp_file.close()
get_questions("datasets/OpenEnded_mscoco_train2014_questions.json")
answer_path="datasets/mscoco_train2014_annotations.json"
get_answers(answer_path)
get_top1000_answers(answer_path)