from keras.layers import *
import json
from PIL import Image
import scipy.misc
from keras.optimizers import SGD
from keras.utils import plot_model
from os import walk
import numpy as np
import warnings
from keras.models import Model,Sequential
from keras.layers import Flatten, Dense, Input
from keras.layers import Convolution2D, MaxPooling2D
from keras.preprocessing import image
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.utils.data_utils import get_file
from keras import backend as K
from keras.utils import plot_model
import keras
from numpy import asarray
from numpy import zeros
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import pickle
from keras.layers import Flatten, Dense, Input
from keras.models import Sequential, Model
from keras.layers.core import Reshape, Activation, Dropout
from keras.layers import LSTM, Merge, Dense, Embedding, Multiply
from keras.models import load_model
from language_model import *
from image_model import *
from config import *

f1=open("subset/preprocessed_images","rb")
images=pickle.load(f1)

f2=open("subset/preprocessed_questions","rb")
questions=pickle.load(f2)

f3=open("subset/preprocessed_answers","rb")
answers=pickle.load(f3)


#-------- COMBINED MODEL----------------
def get_model():
    image_model=get_image_model()
    language_model=get_language_model()
    mergedOut = Multiply()([image_model.output,language_model.output])  
    mergedOut = Dense(1024, kernel_initializer='random_uniform',activation='relu')(mergedOut)
    mergedOut = Dense(1024, kernel_initializer='random_uniform',activation='relu')(mergedOut)
    mergedOut = Dense(1000, activation='softmax')(mergedOut)
    newModel = Model([image_model.input,language_model.input], mergedOut)
    return newModel

#--------------------------Trianing-------------------
print("Starting Training..........")

final_model=get_model()
continue_training=False
n_epochs=config.n_epochs
val_split=config.val_split
image_features=images[:]
max_ques_length = get_max_question_length(questions)
tokenizer = get_tokenizer(questions)  
encoded_ques = get_encoded_questions(tokenizer,questions) 
padded_ques = get_padded_questions(encoded_ques, max_ques_length)
from keras.callbacks import EarlyStopping
early_stopping = EarlyStopping(monitor='val_loss', patience=2)
while True:
  if continue_training:
    n_epochs=int(input("Enter how many epochs you want to increase"))
    tot_epochs+=n_epochs
  final_model.compile(loss='categorical_crossentropy', optimizer='adam',metrics=['accuracy'])
  final_model.fit([np.array(image_features),np.array(padded_ques)],np.array(answers),epochs=n_epochs,validation_split=val_split,callbacks=[early_stopping])
  temp=input("Do you want to continue training?(yes/no)")
  if temp=="yes":
    continue_training=True
    final_model.save("vqa_v2_"+str(n_epochs)+".h5")
  else:
    break
  
final_model.save("vqa_"+config.model+"_"+str(n_epochs)+".h5")
print("Finished Training with ",tot_epochs)
print("\n---------------------------\n")