from image_model import *
from keras.layers import *
import json
from PIL import Image
import scipy.misc
from keras.optimizers import SGD
from keras.utils import plot_model
from os import walk
import numpy as np
import warnings
from keras.models import Model,Sequential
from keras.layers import Flatten, Dense, Input
from keras.layers import Convolution2D, MaxPooling2D
from keras.preprocessing import image
from keras.utils.layer_utils import convert_all_kernels_in_model
from keras.utils.data_utils import get_file
from keras import backend as K
from keras.utils import plot_model
import keras
from numpy import asarray
from numpy import zeros
import os
from keras.preprocessing.sequence import pad_sequences
import pickle
from keras.layers import Flatten, Dense, Input
from keras.models import Sequential, Model
from keras.layers.core import Reshape, Activation, Dropout
from keras.layers import LSTM, Merge, Dense, Embedding, Multiply
from keras.models import load_model

print("Started preprocessing.......")
question_path="subset/q_train2014"
answer_path="subset/ca_train2014"
top_answer_path="subset/top1000answers"
image_path="train2014/COCO_train2014_"
image_format=".jpg"
question_file=open(question_path,"r")
questions_json=json.loads(list(question_file)[0])
questions=[]
answers=[]
images=[]
ques={}
top_answers=open(top_answer_path,"rb")
top_answers=pickle.load(top_answers)
top_answers=top_answers[:1000]
for i in questions_json:
  ques[i["question_id"]]=[(str(i["image_id"]).rjust(12,'0')),i["question"]]
for i in ques:
  ques[i][0]=(image_path+ques[i][0]+image_format)
answer_file=open(answer_path,"r")
answer_json=json.loads(list(answer_file)[0])
for i in answer_json:
  ques[i["question_id"]].append(i["answers"][0])
ques=[[i,ques[i][0],ques[i][1],ques[i][2]] for i in ques]
c=0
cnt=1
print(len(ques))
batch=50
for i in ques:
  img=load_and_scale_imgs(i[1],True)   # with L2 normalization
  if img!=[]:    #is empty when it is a black and white image
      images.append(img[0])
      #os.system("cp "+i[1]+" subset/train_img/.")
      questions.append(i[2])  
      answer=[0 for i in range(1000)]
      if i[3] in top_answers:
        answer[top_answers.index(i[3])]=1
      else:
        answer[-1]=1
      answers.append(answer)
      cnt+=1
  if cnt%batch==0:
        print("finished preprocessing ",cnt," instances")
      #print(cnt)
f1=open("subset/preprocessed_images","wb")
pickle.dump(images,f1)

f2=open("subset/preprocessed_questions","wb")
pickle.dump(questions,f2)

f3=open("subset/preprocessed_answers","wb")
pickle.dump(answers,f3)

print("Finished preprocessing")
