import keras
import config
from keras.models import Model,Sequential
from keras.applications.resnet50 import ResNet50
from keras.applications.vgg16 import VGG16
from keras.applications.xception import Xception

def pop(self):
    '''
      Removes a layer instance on top of the layer stack.
    '''
    if not self.outputs:
        raise Exception('Sequential model cannot be popped: model is empty.')
    else:
        self.layers.pop()
        if not self.layers:
            self.outputs = []
            self.inbound_nodes = []
            self.outbound_nodes = []
        else:
            self.layers[-1].outbound_nodes = []
            self.outputs = [self.layers[-1].output]
        self.built = False

def predict(img_array):
    return image_model.predict(img_array)

if config.model=="vgg16":
    image_model=VGG16(include_top=True, weights='imagenet', input_tensor=None, input_shape=None, pooling=None)  
    pop(image_model)
    pop(image_model)
    image_model.compile(loss='categorical_crossentropy',optimizer="sgd")

elif config.model == "resnet50":
    image_model=ResNet50(weights='imagenet')  
    pop(image_model)
    image_model.compile(loss='categorical_crossentropy',optimizer="sgd")

else:
    image_model=Xception(include_top=True, weights='imagenet')
    pop(image_model)
    image_model.compile(loss='categorical_crossentropy',optimizer="sgd")
image_model.summary()


def load_and_scale_imgs(img_path,norm_image=False):
    imgs=scipy.misc.imresize(scipy.misc.imread(img_path), (224, 224,3))
    imgs=np.array(imgs)
    if imgs.shape==(224,224):
      return []
    imgs=np.expand_dims(imgs, axis=0)
    img_feature = predict(imgs)
    if norm_image:
        tem = np.sqrt(np.sum(np.multiply(img_feature, img_feature)))
        img_feature = np.divide(img_feature, np.tile(tem,(1,4096)))
    return np.array(img_feature)



def get_image_model(num_mlp_units=1024,activation_function="tanh"):
    new_model = Sequential() #new model
    if config.model=="vgg16":
        new_model.add(Reshape((4096,), input_shape=(4096,)))
    else:
        new_model.add(Reshape((2048,), input_shape=(2048,)))

    new_model.add(Dense(num_mlp_units,kernel_initializer='random_uniform'))
    new_model.add(Activation(activation_function))
    #new_model.add(Dropout(0.5))
    return new_model